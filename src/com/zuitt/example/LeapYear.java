package com.zuitt.example;
import java.util.Scanner;
public class LeapYear {
    public static void main(String[] args){
        Scanner year = new Scanner(System.in);

        System.out.println("Input year to be checked if a leap year:");
        int y = year.nextInt();

        if (y % 400 == 0) {
            System.out.println(y + " is a leap year.");
        }
        else if (y % 100 == 0) {
            System.out.println(y + " is not a leap year.");
        }
        else if (y % 4 == 0) {
            System.out.println(y + " is a leap year.");
        }
        else {
            System.out.println(y + " is not a leap year.");
        }
    }
}
